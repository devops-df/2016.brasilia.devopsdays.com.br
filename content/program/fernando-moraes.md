+++
categories = ["Talk", "Palestra"]
copalestrante_name = "Alexandre Maldaner"
copalestrante_bio = "Alexandre é Engenheiro de Computação, formado pela Unicamp, e pós-graduado em Telecomunicações e Engenharia de Redes, pelo Inatel.  Começou sua carreira como Engenheiro de Software em projetos internacionais na área de telecomunicações, com foco no desenvolvimento de soluções de alta performance e robustez em Java.  Ao longo dos anos, passou a atuar em uma grande variedade de tecnologias em diferentes posições técnicas e gerenciais em times de desenvolvimento de software em diversas áreas, incluindo: redes, VoIP, plataformas de mensagens, soluções Web, big data, redes sociais, aplicativos móveis, jogos, videoconferência e virtualização.  Como Diretor de Operações da Daitan, interage com clientes do Brasil, da Europa e do Vale do Silício – onde vem presenciando diariamente casos de sucesso na adoção de DevOps – e acompanha a execução dos projetos para garantir que as diferentes equipes de desenvolvimento tenham todo o suporte e apoio necessários para entregar produtos e serviços de altíssima qualidade."
copalestrante_photoUrl = "https://www.dropbox.com/s/dg5fwc296an7la0/2_Alexandre_Maldaner.png?dl=1"
copalestrante_twitter = ""
copalestrante_website = ""
date = "2016-09-21T19:11:06-03:00"
description = "Fernando Moraes"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Fernando Moraes"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Implementando DevOps em nossos clientes no Vale do Silício: 5 práticas comprovadas para acelerar a mudança de cultura</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Mudança de cultura é um dos maiores desafios para a adoção de DevOps. A Daitan tem contribuído e também aprendido com a adoção de DevOps em nossos clientes no Vale do Silício já há alguns anos, em dezenas de projetos. Nesta conversa, compartilharemos 5 práticas que em nossa experiência prática mostraram-se úteis para aproximar os times de Dev e Ops, desde a construção criteriosa de objetivos para os times, até ferramentas que aumentam a interação e diminuem o atrito entre esses grupos.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture (Cultura)</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-devops-no-vale-do-silicio-fernando-moraes`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-devops-no-vale-do-silicio-fernando-moraes)
