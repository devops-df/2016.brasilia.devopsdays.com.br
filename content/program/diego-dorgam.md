+++
categories = ["Talk", "Palestra"]
copalestrante_name = "Rodrigo Nascimento"
copalestrante_bio = "Desenvolvedor há 10 anos, mora em Taquara - RS. Líder de desenvolvimento na Rocket.Chat e atualmente trabalhando com JavaScript full stack, integrações e performance."
copalestrante_photoUrl = "https://avatars3.githubusercontent.com/u/234261?v=3&s=466"
#copalestrante_twitter = "  "
copalestrante_website = "https://rocket.chat/"
date = "2016-09-21T19:11:06-03:00"
description = "Diego Dorgam"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Diego Dorgam"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">ChatOps com Rocket.Chat</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Apresentar o poder de scripting da ferramenta Rocket.Chat e as possibilidades de integração com Hubot para operar automação em ambientes GitLab.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Culture (Cultura), Automation (Automação)</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-chatops-com-rocket-dot-chat-diego-dorgam`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-chatops-com-rocket-dot-chat-diego-dorgam)
