+++
categories = ["Talk", "Palestra"]
copalestrante_bio = ""
copalestrante_name = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""
date = "2016-09-25T00:46:58-03:00"
description = "Fernando Ike"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Fernando Ike"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Docker Cluster - Swarm</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Falaremos um pouco sobre a produto Swarm da Docker que foi remodelado e agora seu uso de cluster é muito mais simples. Configurar um ambiente de produção nunca foi tão fácil. Falaremos de como iniciar serviços e como subir esse ambiente em poucos comandos.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-docker-swarm-fernando-ike`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-docker-swarm-fernando-ike)
