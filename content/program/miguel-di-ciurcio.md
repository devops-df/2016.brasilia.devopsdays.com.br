+++
categories = ["Talk", "Palestra"]
copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = "http://instruct.com.br/blog"
date = "2016-09-21T19:11:06-03:00"
description = "Miguel Di Ciurcio"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "miguel-di-ciurcio"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Evolução da linguagem do Puppet</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Durante os mais de 10 anos de existência do Puppet, a sua linguagem de modelagem vem evoluindo e acrescentando cada vez mais funcionalidades e recursos.

Veremos como e porque o Puppet foi deixando de lado inspirações vindas da orientação a objetos, partir para **composição** ao invés de **herança** e recebendo influência de linguagens funcionais também.

Além disso, veremos como e porque a implementação do Puppet que inicialmente era apenas em Ruby MRI, hoje utiliza uma combinação de JRuby, Clojure e Java e está passando por uma reimplementação em C++.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-evolucao-do-puppet-miguel-di-ciurcio-filho`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-evolucao-do-puppet-miguel-di-ciurcio-filho)
