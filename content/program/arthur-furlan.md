+++
categories = ["Talk", "Palestra"]
copalestrante_bio = ""
copalestrante_name = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""
date = "2016-11-08T18:39:18-02:00"
description = "Arthur Furlan"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "arthur furlan"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Os desafios de entregar DevOps para leigos</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Todos os desafios que enfrentamos na Configr ao tentar entregar a cultura DevOps para um público leigo.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text text-primary">Culture (Cultura)</span>
<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-os-desafios-de-entregar-devops-para-leigos-arthur-furlan`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-os-desafios-de-entregar-devops-para-leigos-arthur-furlan)
