+++
categories = ["Talk", "Palestra"]
date = "2016-09-05T18:59:19-03:00"
description = "Bárbara Hartmann"
draft = false
tags = ["DevOps", "Automation", "Metrics"]
title = "barbara-hartmann"

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_website = ""
copalestrante_twitter = ""
copalestrante_photoUrl = ""

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
#  <span class="text text-success">Produção nove vezes ao dia - Como um time da *rackspace* torna isso possível?</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Vamos apresentar quais técnicas e ferramentas são usadas no nosso time para garantir que as entregas em produção de um projeto com 5 anos de vida tenham valor e qualidade e sejam feitas 9 vezes ao dia, 5 dias por semana, 12 meses ao ano. Espere ouvir sobre pipeline, testes, automação, revisão de código, branch based development, multiple single page applications, Pikachu, ChatOps, Hackdays e feature toggles.

<!--
Temas das trilhas:
  <span class="text-primary"><b>[C]</b>ulture (Cultura)</span>
  <span class="text-success"><b>[A]</b>utomation (Automação)</span>
  <span class="text-info"><b>[M]</b>etrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning"><b>[S]</b>haring (compartilhamento)</span>
-->

- **Trilha**: <span class="text text-success">Automation (automação)</span>
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-producao-9-vezes-por-dia-barbara-hartmann`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-producao-9-vezes-por-dia-barbara-hartmann)
