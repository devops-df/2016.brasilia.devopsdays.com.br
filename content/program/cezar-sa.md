+++
categories = ["Talk", "Palestra"]
date = "2016-09-21T19:11:06-03:00"
description = "Cezar Sa"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Cezar Sa"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Docker na Globo.com: Usando o tsuru para escalar milhares de containers em centenas de maquinas</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->

O objetivo dessa palestra é mostrar como a Globo.com usa docker como a principal forma de deploy de aplicações para produção usando o PaaS tsuru desenvolvido internamente como um projeto completamente open-source. Vamos ver na prática como o tsuru faz o scheduling de containers Docker e de VMs, como é feito o tratamento automático de falhas e deploys sem nenhum downtime. Além disso, vamos ver um números e informações sobre como a Globo.com usa o tsuru para proporcionar um ambiente de deploy seguro onde quase 1000 containers Docker são recriados diariamente. O tsuru é um PaaS open-source e mais informações sobre ele podem ser encontradas em https://tsuru.io/ e https://github.com/tsuru/tsuru.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture/Automation</span>

<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-tsuru-docker-na-globo-dot-com-cezar-sa`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-tsuru-docker-na-globo-dot-com-cezar-sa)
