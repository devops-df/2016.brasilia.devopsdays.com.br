+++
categories = ["Talk", "Palestra"]
date = "2016-09-21T19:11:06-03:00"
description = "Jonathan Baraldi"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Jonathan Baraldi"

copalestrante_name = "Rômulo Igor Conceição"
copalestrante_bio = "Amazon AWS Cloud Professional, Amazon AWS Security Professional, Software Architect, Python and Cloud Evangelist, CEO and Co-Founder Solid IT Tecnologia da Informação Ltda, CTO and Co-Founder Gabster Serviços em Tecnologia da Informação, Systems Integrator HCPA - Hospital de Clínicas de Porto Alegre"
copalestrante_photoUrl = "http://www.solidit.com.br/marketing/romulo.jpg"
copalestrante_twitter = "romuloigor"
copalestrante_website = ""
+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Começando com tudo: Rancher Convoy-NFS, Proxy, Private Registry e Catalog</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->

Mostrar a importância destes 4 itens itens básicos, que são pilares para o início da adocação da cultura dos containers e do desenvolvimento do processo de DevOps nas organizações.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Culture/Automation</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-private-docker-registry-e-rancher-catalog-jonathan-baraldi`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-private-docker-registry-e-rancher-catalog-jonathan-baraldi)
