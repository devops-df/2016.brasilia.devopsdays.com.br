+++
categories = ["Talk", "Palestra"]
copalestrante_bio = ""
copalestrante_name = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""
date = "2016-09-24T21:53:58-03:00"
description = "Gustavo Hoyer"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "gustavo hoyer"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-info">Gestão centralizada de logs com ELK</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
A palestra consistirá na apresentação de arquiteturas de referência para a implantação de um sistema de gestão centralizada de logs com o ELK abordando vantagens e desvantagens de cada arquitetura, e apresentar o case da Dataprev mostrando a arquitetura adotada, volumes e tipos de logs tratados.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>

<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***:  [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-gestao-centralizada-de-logs-com-elk-gustavo-hoyer`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-gestao-centralizada-de-logs-com-elk-gustavo-hoyer)
