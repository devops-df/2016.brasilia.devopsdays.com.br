+++
categories = ["Talk", "Palestra"]
date = "2016-09-21T19:11:06-03:00"
description = "Matheus Hernandes"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Matheus Hernandes"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">O início da infraestrutura como código no Tribunal de Contas da União</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->

Nesta apresentação iremos compartilhar nossa experiência com "Infra as Code", Cultura DevOps e em especial o processo de automação de nossa infraestrutura utilizando Puppet no Tribunal de Contas da União.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-inicio-da-infraestrutura-como-codigo-no-tcu-matheus-hernandes`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-inicio-da-infraestrutura-como-codigo-no-tcu-matheus-hernandes)
