+++
categories = ["Talk", "Palestra"]
date = "2016-09-21T19:11:06-03:00"
description = "Edson Yanaga"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Edson Yanaga"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">The Deploy Master: From Basic to Zero Downtime, Blue/Green, A/B, and Canary</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->

The “deploy moment” is an occasion that still gives many developers the shivers. But it shouldn’t be this way (at least not every time). Luckily enough, we have tools and processes today that enable us to turn the deploy moment into a usual activity.

Check out this session to learn how we can evolve our Java deployment process from the very basic to zero downtime and then apply some very interesting strategies such as blue/green, A/B, and Canary deployments.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture/Automation</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/b-and-canary-edson-yanaga`](https://speakerdeck.com/devopsdf/b-and-canary-edson-yanaga)
