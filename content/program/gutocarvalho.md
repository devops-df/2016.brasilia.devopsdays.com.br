+++
categories = ["Talk", "Palestra"]
copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""
date = "2016-09-21T19:11:06-03:00"
description = "Guto Carvalho"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "gutocarvalho"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Introdução a Cultura DevOps</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Essa palestra vai iniciar o ouvinte na cultura DevOps, o objetivo é fazer com que os iniciantes entendam como surgiu o termo, quem foram os idealizadores da ideia, quais as motivações para criação de tal movimento, como ele se organiza e também mostrar como funciona a comunidade em volta da iniciativa.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture (Cultura)</span>
<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Iniciante
- ***slides***:  [`https://speakerdeck.com/devopsdf/devopsdays-brasilia-cultura-devops-guto-carvalho`](https://speakerdeck.com/devopsdf/devopsdays-brasilia-cultura-devops-guto-carvalho)
