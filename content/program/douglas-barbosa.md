+++
categories = ["Talk", "Palestra"]
date = "2016-09-21T19:11:06-03:00"
description = "Douglas Barbosa"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Douglas Barbosa"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">GitLab - Faster from Idea to Production</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->

Modern software development process is spread across many tools. Every tool covers a part of the whole process. This talk highlight GitLab’s open set of tools for the software development lifecycle, from idea to production, through chat, issues, planning, merge request, CI, and CD.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Iniciante
