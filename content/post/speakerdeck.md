+++
categories = ["Announcement", "News"]
date = "2016-11-22T23:12:00-02:00"
description = "Aêêê! Disponibilizada estante DevOps para palestras..."
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Registration", "Sharing"]
title = "Apresentações disponibilizadas"

+++

O grupo DevOps-DF <i class="fa fa-handshake-o" aria-hidden="true"></i> criou um espaço para que sejam disponibilizadas as palestras do DevOpsDays Brasília 2016!

Com isso, colocaremos lá os arquivos das apresentações que foram dadas no DevOpsDays Brasília 2016.

Veja na programação do evento ou para cada um de nossos palestrantes o link para as [palestras](/about/programa).

Acesse o link e assista lá os *slides* das apresentações - *classifique como uma estrela* (***Star***) aquelas que gostou.

E **atenção**...

Fique ligado no site para as novidades, pois logo logo iremos publicar infos sobre as gravações e fotos das palestras.
