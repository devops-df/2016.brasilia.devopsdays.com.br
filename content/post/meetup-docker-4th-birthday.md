+++
categories = ["Development", "Operation", "Announcement", "News"]
date = "2017-03-08T14:56:59-03:00"
description = "Os grupos DevOps-DF e Docker-Brasília convidam os entusiastas a uma tarde para compartilharmos experiências nesse mundo conteinerizado"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Docker", "CI/CD", "Lean", "Metrics", "Sharing"]
title = "Docker Birthday #4: o que celebrar?"

+++

Vejam a seguir o dados do *Meetup* que foi preparado pelos grupos DevOps-DF e Docker-Brasília:

- **Local**: Dataprev-DF
  - SAUS, quadra 1, bloco E
  - Sala multiuso, 11o andar
  - Brasília

- **Agenda**: 13/março/2017 de 13:30h às 18:00h
  - **13:30h** Recepção, networking!
  - **14:00h** *Extraindo o máximo dos Containers: casos ideais de uso, vantagens, mitos, cuidados e o que muda no cenário de containers com o Docker Enterprise Editition* - Guto Carvalho
  - **14:30h** *Ambiente de deploy automático com Docker* - Flávio Silva, Felipe Sampaio e Eduardo Santos - Agência Espacial Brasileira
  - **15:00h** *Minimizando riscos e otimizando desempenho em pipeline conteinerizado* - Adriano Vieira
  - **15:30h** *Autobuild: do código à imagem no hub.docker.com em minutos* - Cláudio Filho
  - **16:00h** *lightning talks:*
      - *Docker para freelancers: Focando no problema e não no ambiente* - Douglas Andrade
      - *Rancher trabalhando para o meio ambiente. Como começar?* - Eustáquio Guimarães - Ibama
  - **16:30h** *Como rodar sua plataforma de chat usando docker-compose para criar e administrar o ambiente* - Diego Dorgam
  - **17:00h** Encerramento: um café com biscoitos prum bate-papo final com velhos e novos amigos!

- **Inscrições**:  
  Acesso o formulário para inscrever-se: https://docs.google.com/forms/d/1sXQzfuC4GbuJVnhYIvw1N0ceOf0V-VhuACIY_i6ej4A/


Participe dos grupos de meetup. Links:

- DevOps-DF <https://www.meetup.com/pt-BR/devops-df>
- Docker-Brasília <https://www.meetup.com/pt-BR/Docker-Meetup>
