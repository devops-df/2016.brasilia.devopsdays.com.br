+++
categories = ["Announcement", "News"]
date = "2016-12-01T18:56:58-02:00"
description = ""
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Certificados de Participação"

+++

<iframe
  allowtransparency="true"
  border="0"
  frameborder="0"
  src="http://www.eventize.com.br/eventize/frm_gera_certificado.php?SEQ_EVE=3395"
  style="border:0;margin:0;padding:0;width:100%;background-color: none;height:150px;">
</iframe>

Consideramos emitir o **Certificado de Participação** do evento por ser importante a um evento técnico como foi este **DevOpsDays Brasília 2016**.

Assim, disponibilizamos a partir de hoje a emissão do certificado de participação no **DevOpsDays Brasília 2016**.

Para ter acesso ao seu basta informar o email utilizado na inscrição do evento no link acima. Lembrando que apenas aqueles que tiveram a sua presença confirmada no evento terão acesso ao seu respectivo certificado.

Mais uma vez o nosso muito obrigado pela sua presença.

Caso necessite de alguma informação adicional ou mesmo a correção dos dados do seu certificado, entre em contato com a organização através do email [`organizers-brasilia-2016@devopsdays.org`](mailto:organizers-brasilia-2016@devopsdays.org).

Nos vemos em 2017!
