+++
categories = ["Announcement", "News"]
date = "2016-11-18T20:00:00-02:00"
#PublishDate = "2016-11-18T20:10:00-02:00"
description = "Felizmente está nos planos uma nova versão!"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Registration", "Sharing"]
title = "Até o ano que vem!"

+++

# Acabou!!! <i class="fa fa-frown-o" aria-hidden="true"></i>

Pois é! O DevOpsDays Brasília 2016 está no fim!

E esta primeira versão do evento encerrou com chave de ouro!

Agradecemos a todos os envolvidos pelo bom andamento do evento e pela cooperação. <i class="fa fa-handshake-o" aria-hidden="true"></i>

Acreditamos que foi bastante proveitoso para todos e que aprendemos bastante!  <i class="fa fa-graduation-cap" aria-hidden="true"></i>

Não fiquem tristes, pois está nos planos uma versão 2017 do DevOpsDays Brasília. <i class="fa fa-smile-o fa-2x" aria-hidden="true"></i>

Logo logo... solicitaremos aos participantes que respondam nossa rápida pesquisa de opinião - não deixe de responder, pois será importante como insumo para nossa próxima edição.

Agora é hora de descontrair um pouco e ir lá no *pub* O'Rilley onde teremos a [**After-Party**: ***Confraternização DevOpsDays Brasília!*** <i class="fa fa-question-circle" style="cursor:pointer;"></i>](post/after-party).
