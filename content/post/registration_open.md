+++
categories = ["Announcement", "News"]
date = "2016-09-17T18:22:45-03:00"
description = "Participe também do primeiro evento DevOpsDays de Brasília"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Registration"]
title = "Inscrições abertas"

+++

**Atenção:**
<p class="text-center bg-danger text-warning">As inscrições para o DevOpsDays Brasília 2016 estão finalizadas!</p>

Venha participar do primeiro DevOpsDays de Brasília!

Teremos várias palestras e atividades para um dia inteiro de troca de experiências entre os vários presentes e palestrantes.

## Sobre o programa do evento

Fique antenado com o [programa](/about/programa) que iremos proporcionar nesse que é o primeiro **DevOpsDays** da capital federal.

No momento temos palestrantes confirmados da Docker, RedHat, Chef, Rancher, Rocket Chat, Instruct, Dataprev, TCU e muito mais.

O padrão da grade é muito similar aos eventos internacionais, é uma oportunidade única em Brasília.

## Sobre as inscrições

O valor normal das incrições é de <b>R$ 200,00</b> para pessoa física, essa inscrição é feita pelo sistema [eventize
 <sup><i class="fa fa-external-link"></i></sup>](http://www.devopsdaysbrasilia.eventize.com.br).

Para *pessoas com deficiência*^[*pessoas com deficiência:* considera-se pessoa com deficiência aquela que tem impedimentos de longo prazo de natureza física, mental, intelectual ou sensorial, os quais, em interação com diversas barreiras, podem obstruir sua participação plena e efetiva na sociedade em igualdade de condições com as demais pessoas. - LCP 142/13] será dado **50% de desconto**. Para estes casos entre em contato pelo e-mail <b>organizers-brasilia-2016@devopsdays.org</b> solicitando mais informações.

Se você quiser fazer inscrições em grupo para governo via empenho ou para empresas privadas, o valor por pessoa é de <b>R$ 250,00</b>. Para este tipo é necessário grupo **mínimo de 5 (cinco) inscrições** e elas serão realizadas diretamente com a organização. Entre em contato pelo e-mail <b>organizers-brasilia-2016@devopsdays.org</b> solicitando mais informações.

Para inscrições individuais, use o código de desconto ***DC61975969*** que dará **20% de desconto** para inscrições antecipadas (*Early Bird*). É limitado o número de inscrições com esse código, então não perca tempo.

Se você faz parte das comunidades **Docker-BR, Puppet-BR** ou **GitLab-BR** no telegram, entre nos grupos e procure por cupons especiais com até 50% de desconto na inscrição.

Para detalhes, acesse o link [eventize
 <sup><i class="fa fa-external-link"></i></sup>](http://www.devopsdaysbrasilia.eventize.com.br) e registre-se o quanto antes e aproveite o desconto para inscrições antecipadas.

<small>O ***Eventize*** é um site externo para registro e gerenciamento de inscrições, estamos utilizando este sistema para inscrições no DevOpsDays Brasília.</small>

## Infra do evento

No evento iremos servir coffee break de manhã e de tarde, o centro de convenções conta com wifi, ar-condicionado, som e projeção de alta qualidade para você acompanhar com tranquilidade todas as palestras. Haverá tradução simultânea para as palestras em inglês e máquinas de bebida quente (muito café) a disposição durante todo o evento.

Todas as palestras serão gravadas em qualidade HD e disponibilizadas posteriomente.

Todo o dinheiro arrecado será utilizado para custear a infraestrutura deste evento.

Esse é um evento sem fins lucrativos organizado por comunidades de TI da capital.

Abraços.
