+++
categories = ["Announcement", "News", "Party"]
date = "2016-11-15T15:54:14-02:00"
description = "DevOpsDays Brasília terá evento de confraternização!"
draft = false
image = "img/home-bg.jpg"
tags = ["Culture", "After-Party", "Party", "Sharing"]
title = "After party"

+++

A organização do evento fez uma parceria com o *Pub* O'Rilley^[O'Rilley Irish Pub] e a Banda **BR080**^[[BR080 no facebook](https://www.facebook.com/events/624634414406837/?__mref=message_bubble)]: os participantes que apresentaram o crachá do DevOpsDays Brasília na entrada do PUB ***até as 21 horas*** irão pagar apenas R$10,00 (dez reais) o ingresso.

Caso queriam levar um convidado é necessário que entrem juntos no *Pub*.

Local:

- O'Rilley Irish Pub  
  <small>Endereço: SCLS 409 Bloco C Loja 36 - Brasília/DF</small>
