+++
date = "2016-08-23T10:13:49-03:00"
categories = ["Announcement", "News"]
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Proposals"]
draft = false
image = "img/post-bg.jpg"
title = "Chamada de trabalhos aberta!"
description = "Chamada de trabalhos aberta, envie sua proposta!"

+++

Venha fazer parte do time que cresce a cada dia e participe do evento como palestrante...

Como palestrante você:

- terá entrada franca à conferência;
- aproveitará sua estadia em Brasília;
- será (re)conhecido como referência no tema de sua palestra e como formador de opinião e tendências.

### Template para *slides*

Disponibilizamos também modelo (*template*) para apresentações, disponível em:

- *openoffice/libreoffice*: <span class="btn btn-link"><a href="https://drive.google.com/open?id=0B5LVaY058UyhUGltdExxT0tXSVk" target="blank">ModeloApresentacao.otp</a></span>
- *powerpoint*: <span class="btn btn-link"><a href="https://drive.google.com/open?id=0B5LVaY058UyhcGtoYW4xUFo0Y00" target="blank">ModeloApresentacao.pot</a></span>

Exemplo de apresentação com o modelo acima:

<a class="btn btn-link" href="https://drive.google.com/open?id=0B5LVaY058UyhRlhBUkVDRnRnakE" target="blank">Lorem ipsum-DevOpsDays-2016-Brasília.pdf</a>

Para detalhes, acesse o link [Chamadas (CFP)](/about/cfp) e envie-nos sua prosposta.