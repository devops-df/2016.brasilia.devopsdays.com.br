+++
categories = ["Announcement", "News"]
date = "2016-12-18T12:16:13-02:00"
description = "Disponibilizadas as fotos do nosso último evento"
draft = false
image = "img/event-photo.png"
tags = ["DevOps", "Culture", "Sharing"]
title = "As fotos.."

+++

{{< figure src="/img/event-pictures/organizadores.jpg" title="Os organizadores!" width="100%;" >}}

<hr />

Algumas das fotos...

{{< highlight html >}}
  <!-- Responsive CSS Image Slider
      A Pen By Dudley Storey http://codepen.io/dudleystorey/pen/ehKpi -->
  <style>
    @keyframes slidy {
      0% { left: 0%; }
      20% { left: 0%; }
      25% { left: -100%; }
      45% { left: -100%; }
      50% { left: -200%; }
      70% { left: -200%; }
      75% { left: -300%; }
      95% { left: -300%; }
      100% { left: -400%; }
    }

    body { margin: 0; }
    div#slider { overflow: hidden; }
    div#slider figure img { width: 20%; float: left; }
    div#slider figure {
      position: relative;
      width: 500%;
      margin: 0;
      left: 0;
      text-align: left;
      font-size: 0;
      animation: 30s slidy infinite;
    }
  </style>
{{< /highlight >}}
{{< highlight html >}}
  <base href="/img/event-pictures/">
  <div id="slider">
    <figure>
      <img src="welcome-1.jpg" alt="por Rosivaldo Moreira" title="Credenciamento">
      <img src="welcome-2.jpg" alt="por Rosivaldo Moreira" title="Credenciamento">
      <img src="organizadores.jpg" alt="por Rosivaldo Moreira" title="Os organizadores">
      <img src="31407156495_a014376674_z.jpg" alt="por Rosivaldo Moreira" title="Brindes I">
      <img src="31407156185_f83f9a03c5_z.jpg" alt="por Rosivaldo Moreira" title="Brindes II">
      <img src="31407154955_7cd19d2fb2_z.jpg" alt="por Rosivaldo Moreira" title="Brindes">
      <img src="31407152285_be03f4f2bf_z.jpg" alt="por Rosivaldo Moreira" title="Fishbowl">
      <img src="31370987286_a2e54f2fae_z.jpg" alt="por Rosivaldo Moreira" title="Openspaces II">
      <img src="31370984576_b4487985c4_z.jpg" alt="por Rosivaldo Moreira" title="Nosso palestrante">
      <img src="31370981556_51c338bb51_z.jpg" alt="por Rosivaldo Moreira" title="Coffee break">
      <img src="31370972716_410473dcc1_z.jpg" alt="por Rosivaldo Moreira" title="Nosso palestrante">
      <img src="31370954966_77cb529166_z.jpg" alt="por Rosivaldo Moreira" title="A audiência">
      <img src="31292404801_5de367c527_z.jpg" alt="por Rosivaldo Moreira" title="A audiência">
      <img src="31292384701_42be30cbc3_z.jpg" alt="por Rosivaldo Moreira" title="Nosso palestrante">
      <img src="31262864482_244d2cfe33_z.jpg" alt="por Rosivaldo Moreira" title="Openspaces I">
      <img src="31262835652_eea48783a6_z.jpg" alt="por Rosivaldo Moreira" title="Nossa palestrante">
      <img src="31262832232_eeb401e2e3_z.jpg" alt="por Rosivaldo Moreira" title="Coffee break">
      <img src="31262822032_294be1792b_z.jpg" alt="por Rosivaldo Moreira" title="Nossos patrocinadores: Puppet, Rancher, Gitlab, Instruct, Redhat">
      <img src="31262815552_d73ef94e80_z.jpg" alt="por Rosivaldo Moreira" title="Ignites">
      <img src="30599921193_461738fb51_z.jpg" alt="por Rosivaldo Moreira" title="A audiência">
      <img src="30599917183_f85f2bf193_z.jpg" alt="por Rosivaldo Moreira" title="A audiência">
      <img src="30599902733_76a16cdfa2_z.jpg" alt="por Rosivaldo Moreira" title="Nosso palestrante">
      <img src="by_Yanaga.jpg" alt="by Edson Yanaga" title="by Edson Yanaga">
    </figure>
  </div>
{{< /highlight >}}

Para ver mais, acesse a página [DevOps-DF no flickr](https://www.flickr.com/photos/devops-df/albums/72157673480572704)

Legal, né?
