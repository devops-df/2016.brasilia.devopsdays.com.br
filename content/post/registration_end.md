+++
categories = ["Announcement", "News"]
date = "2016-11-15T15:54:14-02:00"
description = "Infelizmente precisamos encerrar as inscrições!"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Registration", "Sharing"]
title = "Inscrições encerradas"

+++

As inscrições foram somente on-line. ***Não teremos inscrições no local e, principalmente, no dia do evento!***

Percebemos com o volume de inscrições e de procura que acertamos em proporcionar o **I** (*primeiro*) ***DevOpsDays*** do DF e Região Centro-Oeste.

Alcançamos a meta proposta de 300 (trezentas) inscrições para o DevOpsDays Brasília 2016.

Esperamos que todos os participantes aproveitem as palestras e atividades que irão acontecer ao longo do evento.

Nos encontramos no evento!
