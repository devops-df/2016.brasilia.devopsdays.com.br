+++
date = "2016-10-10T16:09:49-03:00"
PublishDate = "2016-10-10T00:13:49-03:00"
categories = ["Announcement", "News"]
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Proposals"]
draft = false
image = "img/post-bg.jpg"
title = "Chamada de trabalhos encerrada"
description = "É! Realmente encerramos a chamada de trabalhos!"

+++

Obtivemos vários ótimos retornos e nossa grade está completa.

Ainda assim contamos que você venha fazer parte do time e que participe do evento.

Veja nosso [programa de palestras](/about/programa) e a espetacular lista de [palestrantes](/speakers) .

Nos encontramos no evento.
