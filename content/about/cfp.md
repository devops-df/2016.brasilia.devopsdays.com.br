+++
categories = ["Development", "Operation"]
date = "2016-08-24T10:42:39-03:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Chamadas de Trabalhos"
description = "Compartilhe seu conhecimento"

+++
<!--# Chamadas de Trabalhos

***<p class="text-center bg-success text-info">Compartilhe seu conhecimento</p>*** -->

Venha ao ***DevOpsDays Brasília*** e apresente sua palestra para centenas de entusiastas DevOps.

A chamada está aberta e, ao enviar sua proposta, você pode ser um de nossos palestrantes nas várias modalidades de sessões que teremos.

---

**Atenção:**
<p class="text-center bg-danger text-warning">A submissão de propostas está finalizada!</p>

---

Como palestrante você:

- terá entrada franca à conferência;
- aproveitará sua estadia em Brasília;
- será (re)conhecido como referência no tema de sua palestra e como formador de opinião e tendências;

Teremos três formas de sessões:

1. Palestra de 25 minutos
1. Debate ou painel de 25 minutos
1. Mini-Palestra de 12 minutos

Durante o evento também teremos espaços para:

1. **Ignições**: serão apresentadas durante as sessões *ignite*^[***ignite***: <https://www.devopsdays.org/ignite-talks-format/>]. Serão 4 sessões com até 7 minutos cada e com slides trocando automaticamente. Estes ignites acontecerão no auditório principal.
1. **Sessões de Espaço Aberto**: As sessões iniciadas nos *ignites* serão levadas para os espaços abertos (*open spaces*^[***open spaces***: <https://www.devopsdays.org/pages/open-space-format>]) onde a discussão irá se aprofundar naqueles temas.


### **Dicas para quem está submentendo trabalhos**

Dentre os critérios que utilizaremos para a seleção destacamos:

1. Originalidade do tema
1. Conexão com os princípios DevOps
1. Apresentação de cases interessantes e reais de DevOps em organizações
1. Conhecimento sobre o tema proposto

DevOpsDays Brasília incentiva:

- O surgimento de novos palestrantes; submeta sem receio
- A participação ampla de todos na grade
- A inclusão de comunidades com baixa representatividade em nossa grade de palestras.

### Conteúdo das propostas:

- Faça uma descrição rica em detalhes para que possamos compreender sua proposta e avaliar de forma justa o seu trabalho.
- Múltiplas propostas são bem vindas, envie quantas propostas quiser.

### Indicações:

Caso queria indicar um palestrante fique a vontade para nos contatar no endereço <span class="btn btn-link">[organizers-brasilia-2016@devopsdays.org](<mailto: organizers-brasilia-2016@devopsdays.org>)</span>

### Template para *slides*

Criamos modelo (*template*) para apresentações, disponível em:

- *openoffice/libreoffice*: <span class="btn btn-link"><a href="https://drive.google.com/open?id=0B5LVaY058UyhUGltdExxT0tXSVk" target="blank">ModeloApresentacao.otp</a></span>
- *powerpoint*: <span class="btn btn-link"><a href="https://drive.google.com/open?id=0B5LVaY058UyhcGtoYW4xUFo0Y00" target="blank">ModeloApresentacao.pot</a></span>

<small>*Faça download de um dos formatos acima para o caso de considerarem usar o template*</small>.

Exemplo de apresentação com o modelo acima:

<a class="btn btn-link" href="https://drive.google.com/open?id=0B5LVaY058UyhRlhBUkVDRnRnakE" target="blank">Lorem ipsum-DevOpsDays-2016-Brasília.pdf</a>
