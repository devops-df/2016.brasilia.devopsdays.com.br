+++
categories = ["Development", "Operation"]
date = "2016-08-25T23:01:23-03:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Location"]
title = "Local do evento"
description = ""

+++

# Local

### Centro de Convenções do Parque Cidade Corporate

**Endereço**:  

- *Centro de Convenções*  
  Edifício Parque Cidade Corporate  
  Setor Comercial Sul (SCS), quadra 9, bloco A,  
  Asa Sul, Brasília/DF

O *Centro de Convenções* está localizado na Asa Sul, *Plano Piloto* de Brasília/DF tendo próximo a ele:

- Hotéis (no Setor Hoteleiro Sul/SHS)
- Restaurantes e shoppings (no Setor Comercial Sul/SCS)
- Pontos de interesse (como o Parque da Cidade, Torre de TV, Feira de Artesanato/*Feira da Torre*, Esplanada dos Ministérios, Praça dos Três Poderes etc)
- Hospitais (como o Setor Médico Hospitalar Sul/SMHS)
- e no prédio do evento há: restaurantes e estacionamento pago (*estamos tentando, mas por enquanto não há previsão de descontos para participantes do evento*)

<hr />
Mostramos abaixo um mapa reduzido com refência à localização:

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="border: 1px solid black"
  src="http://www.openstreetmap.org/export/embed.html?bbox=-47.89787471294403%2C-15.796930231282808%2C-47.88891613483429%2C-15.792336161337335&amp;layer=mapnik&amp;marker=-15.794633209334844%2C-47.89339542388916">
</iframe>
<br/>
<small>
  <a class="btn btn-link" target="blank" href="http://www.openstreetmap.org/?mlat=-15.79463&amp;mlon=-47.89340#map=18/-15.79463/-47.89340">
    Ver Mapa Ampliado
  </a>
</small>
