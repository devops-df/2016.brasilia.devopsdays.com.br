+++
categories = ["Development", "Operation"]
date = "2016-08-26T20:48:04-04:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "O programa"
description = ""

+++

Teremos três formas de sessões:

1. Palestra de 25 minutos
1. Debate ou painel de 25 minutos
1. Mini-Palestra de 12 minutos

Durante o evento também teremos espaços para:

- **Ignições**: serão apresentadas durante as sessões *ignite*^[***ignite***: <https://www.devopsdays.org/ignite-talks-format/>]. Serão 4 sessões com até  7 minutos cada e com slides trocando automaticamente. Estes ignites acontecerão no auditório principal.
- **Sessões de Espaço Aberto**: As sessões iniciadas nos *ignites* serão levadas para os espaços abertos (*open spaces*^[***open spaces***: <https://www.devopsdays.org/pages/open-space-format>]) onde a discussão irá se aprofundar naqueles temas.
- **Roda Viva**: Oportunidades de debates de ampla participação do plublico presente como debatedor presente no centro da Roda (Open Fishbowl^[***Fishbowl***: <https://en.wikipedia.org/wiki/Fishbowl_(conversation)>])


<div class = "row">
  <div class = "col-md-4">
    <h3>Temas das trilhas</h3>
  </div>
</div>
<div class = "row">
  <div class = "col-md-2">
  </div>
  <div class = "col-md-8">
    <b>CAMS</b>:
      <ul>
        <li class="text-primary"><b>[C]</b>ulture (Cultura)</li>
        <li class="text-success"><b>[A]</b>utomation (Automação)</li>
        <li class="text-info"><b>[M]</b>etrics (métricas, monitoramento, gestão)</li>
        <li class="text-warning"><b>[S]</b>haring (compartilhamento)</li>
      </ul>
  </div>
</div>

<div class = "row">
  <div class = "col-md-11">

    <h3>1<sup>o</sup> dia - 18 novembro</h3>

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-xs-12 col-sm-6 col-md-8 col-md-2 col-md-offset-0">
        <time>08:00-08:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Credenciamento
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>08:30-09:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        Abertura
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>09:00-09:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 1</i> <b class="text text-primary">[C]</b>: <br />
        <a href="/program/gutocarvalho" class="text text-primary">
          Cultura DevOps<br />
          <b>Palestrante:</b> Guto Carvalho <br />
          <b>Empresa:</b> Instruct<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>09:30-10:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Coffee-break
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>10:00-10:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 2</i> <b class="text text-primary">[C]</b>:<br/>
        <a href="/program/fernando-moraes" class="text text-primary">
          Implementando DevOps em nossos clientes no Vale do Silício: 5 práticas comprovadas para acelerar a mudança de cultura<br />
          <b>Palestrante:</b> Fernando Moraes<br />
          <b>Co-Palestrante:</b> Alexandre Maldaner<br />
          <b>Empresa:</b> Daitan Group
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>10:30-11:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 3</i> <b class="text text-success">[A]</b>: <br />
        <a href="/program/barbara-hartmann" class="text text-success">
          Produção nove vezes ao dia - Como um time da rackspace torna isso possível?<br />
          <b>Palestrante:</b> Bárbara Hartmann<br />
          <b>Empresa:</b> ThoughtWorks<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>11:00-11:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 4</i> <b class="text text-success">[A]</b>:<br/>
        <a href="/program/douglas-barbosa" class="text text-success">
          GitLab - Faster from Idea to Production <br />
          <b>Palestrante: </b>Douglas Barbosa Alexandre<br />
          <b>Empresa:</b> GitLab Inc.<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>11:30-12:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 5</i> <b class="text text-primary">[C]</b>:<br/>
        <a href="/program/arthur-furlan" class="text text-primary">
          Os desafios de entregar DevOps para leigos<br />
          <b>Palestrante: </b>Arthur Furlan<br />
          <b>Empresa:</b> Configr<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>12:00-13:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Almoço
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>13:30-14:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 6</i> <b class="text text-success">[A]</b>:<br/>
        <a href="/program/fernando-ike" class="text text-success">
          Docker Cluster - Swarm<br />
          <b>Palestrante: </b>Fernando Ike<br />
          <b>Empresa:</b> Highwinds Network Group, Inc. <br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>14:00-14:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 7</i> <b class="text text-info">[M]</b>:<br/>
        <a href="/program/edson-yanaga" class="text text-info">
          The Deploy Master: From Basic to Zero Downtime, Blue/Green, A/B, and Canary<br />
          <b>Palestrante: </b>Edson Yanaga<br />
          <b>Empresa:</b> RedHat Inc.<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>14:30-15:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 8</i> <b class="text text-info">[M]</b>:<br/>
        <a href="/program/gustavo-hoyer" class="text text-info">
          Gestão centralizada de logs com ELK<br />
          <b>Palestrante: </b>Gustavo Hoyer<br />
          <b>Empresa:</b> Dataprev<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>15:00-15:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 9</i> <b class="text text-success">[A]</b>:<br/>
        <a href="/program/matheus-hernandes" class="text text-success">
          O início da infraestrutura como código no Tribunal de Contas da União<br />
          <b>Palestrante: </b>Matheus Petronillio Hernandes<br />
          <b>Empresa:</b> TCU<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>15:30-16:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 10</i> <b class="text text-success">[A]</b>:<br/>
        <a href="/program/jonathan-baraldi" class="text text-success">
          Começando com tudo: Rancher Convoy-NFS, Proxy, Private Registry e Catalog<br />
          <b>Palestrante: </b>Jonathan Baraldi<br />
          <b>Empresa:</b> Rancher Inc | BRCloudServices<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>16:00-16:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 11</i> <b class="text text-success">[A]</b>:<br/>
        <a href="/program/diego-dorgam" class="text text-success">
          ChatOps com Rocket.Chat<br />
          <b>Palestrante: </b>Diego Dorgam<br />
          <b>Co-Palestrante: </b>Rodrigo Nascimento<br />
          <b>Empresa:</b> Rocket.Chat<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>16:30-17:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->

      </div>
      <div class = "col-md-8 box">
        <i>Coffee Break</b></i><br>

      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
     <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>17:00-17:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 12</i> <b class="text text-success">[A]</b>:<br/>
        <a href="/program/cezar-sa" class="text text-success">
          Docker na Globo.com: Usando o tsuru para escalar milhares de containers em centenas de maquinas<br />
          <b>Palestrante: </b>Cézar Sá Spinola<br />
          <b>Empresa:</b> Globo.com<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
     <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>17:30-18:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Slot 13</i> <b class="text text-success">[A]</b>:<br/>
        <a href="/program/miguel-di-ciurcio" class="text text-success">
          Evolução da linguagem do Puppet<br />
          <b>Palestrante:</b> Miguel Di Ciurcio Filho<br />
          <b>Empresa:</b> Puppet.com | Instruct
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>18:00-18:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Ignites</i>:
        <br /><br />
        <div class = "row">
          <div class = "col-md-14">
            <ol>
              <li>
                <i class="text-warning">Baleias (Docker) versus Foguetes (RKT)</i>
                <i class="fa fa-question-circle text-warning" style="cursor:pointer;" aria-hidden="true" data-toggle="collapse" data-target="#ignite1"></i><br/>
                <div class="collapse" id="ignite1">
                  <i class="small">
                    "Comparar sistema de containers (engine) Docker e Rkt. São dois grandes projetos, com abordagens diferentes que realmente vale comparar qual é mais adequado para você".
                    <br/>
                    <a href="https://speakerdeck.com/devopsdf/openspace-baleias-docker-versus-foguetes-rkt-fernando-ike" target="blank">
                      <b><i>slides</i></b>:
                      <code>https://speakerdeck.com/devopsdf/openspace-baleias-docker-versus-foguetes-rkt-fernando-ike</code>
                    </a>
                  </i>
                </div>
                Fernando Ike
              </li>
              <li>
                <i class="text-warning">OpenShift Intro</i>
                <i class="fa fa-question-circle text-warning" style="cursor:pointer;" aria-hidden="true" data-toggle="collapse" data-target="#ignite2"></i><br/>
                <div class="collapse" id="ignite2">
                  <i class="small">
                    Uma discussão sobre OpenShift e suas principais funcionalidades.
                    <br/>
                    <a href="https://speakerdeck.com/devopsdf/openspace-openshift-intro-gustavo-luszczynski" target="blank">
                      <b><i>slides</i></b>:
                      <code>https://speakerdeck.com/devopsdf/openspace-openshift-intro-gustavo-luszczynski</code>
                    </a>
                  </i>
                </div>
                Gustavo Luszczynski
              </li>
              <li>
                <i class="text-warning">GitLab CI + Rancher</i>
                <i class="fa fa-question-circle text-warning" style="cursor:pointer;" aria-hidden="true" data-toggle="collapse" data-target="#ignite3"></i><br/>
                <div class="collapse" id="ignite3">
                  <i class="small">
                    Pipeline de integração contínua do GitLab (GitLab-CI) integrado à plataforma de gerenciamento de containers Rancher.
                    <br/>
                    <a href="https://speakerdeck.com/devopsdf/openspace-gitlab-ci-plus-rancher-adriano-vieira" target="blank">
                      <b><i>slides</i></b>:
                      <code>https://speakerdeck.com/devopsdf/openspace-gitlab-ci-plus-rancher-adriano-vieira</code>
                    </a>
                  </i>
                </div>                
                Adriano Vieira
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>18:30-19:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Salas externas
      </div>
      <div class = "col-md-8 box">
        <i>Open spaces <b></b></i>:
        <br /><br />
        <ol>
          <li>
            <i class="text-warning">Baleias (Docker) versus Foguetes (RKT)</i><br/>
          </li>
          <li>
            <i class="text-warning">OpenShift Intro</i><br/>
          </li>
          <li>
            <i class="text-warning">GitLab CI + Rancher</i><br/>
          </li>
        </ol>
      </div>
    </div> <!-- end timeslot div -->

<!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>19:00-19:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Roda viva (Open Fishbowl)</i>:
      </div>
    </div> <!-- end timeslot div -->


    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>19:30-20:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        Encerramento e informes
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time><small>20:30-21:00+</small></time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        O'Rilley Irish Pub
      </div>
      <div class = "col-md-8 box">
        <a href="/post/after-party" class="text text-warning">
          <b>After-Party</b>: confraternização DevOpsDays Brasília!
          <i class="fa fa-question-circle" style="cursor:pointer;"></i>
        </a>
      </div>
    </div> <!-- end timeslot div -->

  </div><!-- end day 1 -->

</div>

<small>Mudanças na grade podem ocorrer decorrentes de circunstâncias fora do controle desta organização. Em especial
quando for algo relacionado a agenda de trabalho de palestrantes ou questões de ordem pessoal dos mesmos. Nestes casos a organização vai remanejar o slot inserindo outro conteúdo no mesmo horário.</small>
