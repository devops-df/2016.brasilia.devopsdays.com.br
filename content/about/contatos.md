+++
categories = ["Development", "Operation"]
date = "2016-08-23T11:30:05-03:00"
description = ""
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "contatos"
email_organizers = "organizers-brasilia-2016@devopsdays.org"

+++

## Equipe Local

<ul>
  <li>
    Guto Carvalho
    - <a class="btn-link" href="http://twitter.com/gutocarvalho">@gutocarvalho</a>
  </li>

  <li>
    Taciano Tres
    - <a class="btn-link" href="http://twitter.com/tacianot">@tacianot</a>
  </li>

  <li>
    Adriano Vieira
    - <a class="btn-link" href="http://twitter.com/adriano_vieira">@adriano_vieira</a>
  </li>

  <li>
    Rafael Gomes
    - <a class="btn-link" href="http://twitter.com/gomex">@gomex</a>
  </li>

  <li>
    Lauro Silveira
    - <a class="btn-link" href="http://twitter.com/laurosn">@laurosn</a>
  </li>
</ul>

Sigam-nos lá no *Twitter* [`https://twitter.com/devopsdaysbsb`](https://twitter.com/devopsdaysbsb) e fiquemos antenados aos acontecimentos.

## Time de Divulgação e Captação

Agradecemos os esforços de:

Aline Hubner, Dirceu Silva, Eustáquio Guimarães, Diego Aguilera, Rogério Fernandes Pereira

Caso você queira entrar em contato conosco:

- pelo email: [<i class="fa fa-envelope-o"></i>`organizers-brasilia-2016@devopsdays.org`](mailto: organizers-brasilia-2016@devopsdays.org)
- via chat (*Telegram*): [<i class="fa fa-question-circle-o"></i>`https://telegram.me/DevOpsDaysBrasilia2016`](https://telegram.me/DevOpsDaysBrasilia2016)
